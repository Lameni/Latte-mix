# Latte-mix
Latte-mix is a customizable desktop environment that uses openbox as its window manager.

![Alt-Screenshot](screenshot.png)

## Components
1. openbox
2. tint2
3. thunar
4. rofi
5. feh
6. obconf
7. terminator
8. sxhkd
9. plank

## Theme
[Adapta-Nokto](https://github.com/adapta-project/adapta-gtk-theme)

## Wallpapers
All wallpapers were taken from [Unsplash](https://unsplash.com)

## Installing components
**Debian/Ubuntu/Linux Mint/LMDE:**

``sudo apt install openbox tint2 thunar rofi adapta-gtk-theme feh obconf xorg terminator sxhkd plank``

**Fedora/RHEL:**

``sudo dnf install openbox tint2 thunar rofi adapta-gtk-theme feh obconf xorg-x11-server-Xorg terminator sxhkd plank``
